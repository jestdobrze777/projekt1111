let output = document.querySelector("#output")
let operation = null
let pendingClear = false

const calculatorFunctions = {
    "+": (a, b) => a+b,
    "-": (a, b) => a-b,
    "÷": (a, b) => a/b,
    "×": (a, b) => a*b
}

document.querySelectorAll("#calculator .number")
    .forEach(button => {
        button.onclick = event => {
            if (pendingClear) {
                output.value = ""
                pendingClear = false
            }

            output.value += event.currentTarget.innerText
            console.log(event.currentTarget.innerText)
        }
    })

document.querySelectorAll("#calculator .operation")
    .forEach(button => {
        button.onclick = event => {
            if (pendingClear || output.value.length === 0) return

            const operand = event.currentTarget.innerText

            if (operation != null) {
                output.value = output.value.replace(operation, operand)
            } else {
                output.value += operand
            }

            operation = operand
        }
    })

document.querySelector("#calculator .equal").onclick = _ => {
    if (operation == null) return

    const [firstNumber, secondNumber] = output.value.split(operation)
    output.value = calculatorFunctions[operation](parseInt(firstNumber), parseInt(secondNumber)).toString()

    pendingClear = true
    operation = null
}
